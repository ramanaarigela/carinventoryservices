<?php
class manufacturer{
     
    // database connection 
    private $conn;
     
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    
    public function addManufacturer( $nametxt )
    {
        $query = "INSERT INTO tblmanufacturer(name,createddt) VALUES (:name,:datetxt ) ";
        $stmt = $this->conn->prepare($query);
        $datetxt =  date('Y-m-d h:i:s');
   
        $stmt->bindParam(':name' , $nametxt );   
        $stmt->bindParam(':datetxt' , $datetxt );

        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function getManufacturer( $manfid )
    {
        if($manfid > 0)
        {
            $query = "select * from tblmanufacturer where id = :id ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id' , $manfid );
        }
        else
        {
            $query = "select * from tblmanufacturer";
            $stmt = $this->conn->prepare($query);
        }
        
        if($stmt->execute()){
            return $stmt;
        }else{
            return false;
        }
    }
    

            
  
}