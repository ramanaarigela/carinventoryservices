<?php
class carmodel {
    // database connection 
    private $conn;
     
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    
    public function addCarModel( $dataarr )
    {
        $query = "INSERT INTO tblcarmodel( name, manufid, color, manufyear, regnum, note, img1, img2, quntity, createddt)
                 VALUES (:name, :manufid, :color, :manufyear, :regnum, :note, :img1, :img2, :quntity, :createddt ) ";
        $stmt = $this->conn->prepare($query);
        $datetxt =  date('Y-m-d h:i:s');
    
        $stmt->bindParam(':name' , $dataarr->name );   
        $stmt->bindParam(':manufid' , $dataarr->manfid );
        $stmt->bindParam(':color' , $dataarr->color );
        $stmt->bindParam(':manufyear' , $dataarr->manfyear );
        $stmt->bindParam(':regnum' , $dataarr->regnum );
        $stmt->bindParam(':note' , $dataarr->notes );
        
        $stmt->bindParam(':img1' , $dataarr->img1 );   
        $stmt->bindParam(':img2' , $dataarr->img2 );
        $stmt->bindParam(':quntity' , $dataarr->count );
        $stmt->bindParam(':createddt' , $datetxt );

        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function getCarModels( )
    {       
        $query = "select b.name as manfname , a.* from tblcarmodel a
                        left outer join tblmanufacturer b on (a.manufid = b.id) WHERE a.quntity > 0";
        $stmt = $this->conn->prepare($query);
        
        if($stmt->execute()){
            return $stmt;
        }else{
            return false;
        }
    }
    public function soldCarModel( $modelid)
    {       
        $query = "UPDATE tblcarmodel set quntity = 0 where id = :modelid";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':modelid' , $modelid );
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
}
