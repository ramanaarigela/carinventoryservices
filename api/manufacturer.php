<?php 
include_once '../classes/database.php';
include_once '../classes/manufacturer.php';
 
$database = new Database();
$db = $database->getConnection(); 

$man = new manufacturer($db);

header("Access-Control-Allow-Origin: *"); 
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$action = $request->action ;

if($action=="addmanufacturer")
{ 
   $insertdata = $request->manufacturer ;
   $queryresult = $man->addManufacturer($insertdata);
   echo json_encode($queryresult);
}

else if($action=="readmanufacturers")
{
   $manfid =$request->manfid;
   $result = array(); 
   
    $queryresult = $man->getManufacturer($manfid);
      
    $step = $queryresult->fetchAll(PDO::FETCH_ASSOC); 
    
    foreach ($step  as $row) { 
        $result[] = array(
                        'id' => $row['id'],
                        'name' => $row['name'],
                        'createddt' => $row['createddt']
                    );
     }
       
    echo json_encode($result);
}
