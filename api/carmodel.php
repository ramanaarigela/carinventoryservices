<?php 
include_once '../classes/database.php';
include_once '../classes/carmodel.php';
 
$database = new Database();
$db = $database->getConnection(); 

$carmodel = new carmodel($db);

header("Access-Control-Allow-Origin: *"); 
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$action = $request->action ;

if($action=="addcarmodel")
{ 
   $insertdata = $request->modeldata ;
   $queryresult = $carmodel->addCarModel($insertdata);
//   var_dump($insertdata);
   echo json_encode($queryresult);
}

else if($action=="readcarmodels")
{
    $result = array(); 
   
    $queryresult = $carmodel->getCarModels();
      
    $step = $queryresult->fetchAll(PDO::FETCH_ASSOC); 
    foreach ($step  as $row) { 
        $result[] = array(
                        'id' => $row['id'],
                        'name' => $row['name'],
                        'manfname' => $row['manfname'],
                        'manufid' => $row['manufid'],
                        'color' => $row['color'],
                        'manufyear' => $row['manufyear'],
                        'regnum' => $row['regnum'],
                        'note' => $row['note'],
                        'quntity' => $row['quntity'],
                        'img1' => $row['img1'],
                        'img2' => $row['img2'],
                        'createddt' => $row['createddt']
                    );
     }
       
    echo json_encode($result);
}

else if($action=="soldmodel")
{
    $result = array(); 
    $modelid = $request->modelid ;
    $queryresult = $carmodel->soldCarModel($modelid);
    echo json_encode($queryresult);  
    
}
